# Custom GDPR

## Introduction

To ease GDPR integration in all our games we use a custom popup that fulfil most of tracking requirements  used by our SDK.

## Remote Config Settings :

GdprType :  0  
GdprDisplay :   0 = Force Accept | 1 = Flexible choices

## Integration Steps

1) **"Install"** or **"Upload"** FG CustomGDPR plugin from the FunGames Integration Manager in Unity, or download it from here.

2) Click on the **"Prefabs and Settings"** button from FunGames Integration window to fill up your scene with required components and create the Settings asset.

The CustomGDPR prefab contains a canvas which is used to display the GDPR form (child object “Container”). This window uses TextMeshPro to display info, so you might need to import **TMP Package** (from Package Manager) and **TMP Essentials** (from the toolbar _Window>TextMeshPro>Import TMP Essential Resources_) in your project before you build your app.

<img src="_source/customGdpr.png" width="256" height="540" style="display: block; margin: 0 auto"/>